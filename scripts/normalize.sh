#!/usr/bin/env sh

set -ex

NORMALIZE_VERSION=8.0.1
NORMALIZE_SHA=54f90b09151533dc48fcef71ea4d2a6ca1355d73f662e73b2a574fec4f01d86b
NORMALIZE_ZIP=normalize-"$NORMALIZE_VERSION".zip
NORMALIZE_NAME=normalize.css-"$NORMALIZE_VERSION"
NORMALIZE_URL=https://github.com/necolas/normalize.css/archive/"$NORMALIZE_VERSION".zip
NORMALIZE_TMP=./tmp

rm -rf "$NORMALIZE_TMP"
mkdir -p "$NORMALIZE_TMP"
wget "$NORMALIZE_URL" -O "$NORMALIZE_TMP"/"$NORMALIZE_ZIP"
sha256sum "$NORMALIZE_TMP"/"$NORMALIZE_ZIP"
echo "$NORMALIZE_SHA  $NORMALIZE_TMP/$NORMALIZE_ZIP" | sha256sum -c -
unzip "$NORMALIZE_TMP"/"$NORMALIZE_ZIP" -d "$NORMALIZE_TMP"

csso                                                       \
  --input "$NORMALIZE_TMP"/"$NORMALIZE_NAME"/normalize.css \
  --output assets/normalize-"$NORMALIZE_VERSION".min.css
