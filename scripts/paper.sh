#!/usr/bin/env sh

set -ex

PAPER_VERSION=0.0.1-preview5
PAPER_SHA=a3dbecc0a7998b9503e24342c710919c81a201616f84ce1445ec32fa1ff9a61c
PAPER_ZIP=true-paper-css-"$PAPER_VERSION".zip
PAPER_NAME=true-paper-css-"$PAPER_VERSION"
PAPER_URL=https://gitlab.com/sdwolfz/true-paper-css/-/archive/"$PAPER_VERSION"/"$PAPER_ZIP"
PAPER_TMP=./tmp

rm -rf "$PAPER_TMP"
mkdir -p "$PAPER_TMP"
wget "$PAPER_URL" -O "$PAPER_TMP"/"$PAPER_ZIP"
sha256sum "$PAPER_TMP"/"$PAPER_ZIP"
echo "$PAPER_SHA  $PAPER_TMP/$PAPER_ZIP" | sha256sum -c -
unzip "$PAPER_TMP"/"$PAPER_ZIP" -d "$PAPER_TMP"

cp "$PAPER_TMP"/"$PAPER_NAME"/src/true-paper.min.css assets/"$PAPER_NAME".min.css
