# Curriculum Vitae

Setup to build and publish my CV.

## Requirements

You need the following packages installed:

| Name          | Link                                                              |
|---------------|-------------------------------------------------------------------|
| `bash`        | https://www.gnu.org/software/bash/                                |
| `docker`      | https://docs.docker.com/engine/install/                           |
| `make`        | https://www.gnu.org/software/make/                                |
| `xdg-utils`   | https://freedesktop.org/wiki/Software/xdg-utils/                  |

## Development

### Chromium

Either have chromium installed on your system or use the docker image:

```sh
make shell
```

### Make

```sh
make shell/build

make build
make build/card
make build/cv

make open
make open/card
make open/cv

make clean
make clean/card
make clean/cv
```

## License

Proprietary!
