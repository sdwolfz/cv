# ------------------------------------------------------------------------------
# Settings
# ------------------------------------------------------------------------------

# By default the `help` goal is executed.
.DEFAULT_GLOBAL := help

# Location of this `Makefile`.
HERE := $(realpath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))

# Run `docker` for the current user and directory.
DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v $(HERE):/here -w /here

# Common flags for `chromium` commands.
CHROMIUM_FLAGS :=                         \
	--headless                              \
	--disable-gpu                           \
	--run-all-compositor-stages-before-draw \
	--virtual-time-budget=10000

# ------------------------------------------------------------------------------
# Utility goals
# ------------------------------------------------------------------------------

# # help
#
# Displays a help message containing usage instructions and a list of available
# goals.
#
# Usage:
#
# To display the help message run:
# ```sh
# make help
# ```
#
# This is also the default goal so you can run it as following:
# ```sh
# make
# ```
.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, ...] [<VARIABLE_1>=value_1, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make help'
	@echo ''
	@echo 'Generic goals:'
	@echo '  - help: Displays this help message.'
	@echo '  - ci:   Run the entire CI setup using docker'
	@echo ''
	@echo 'Shell goals:'
	@echo '  - shell:       Open a shell in the `chromium` docker image.'
	@echo '  - shell/build: Chains `build` inside a chromium docker container.'
	@echo '  - shell/css:   Open a shell in the `csso-cli` docker image.'
	@echo ''
	@echo 'Build goals:'
	@echo '  - build:      Generates all PDF files.'
	@echo '  - build/card: Generates the `card` PDF file.'
	@echo '  - build/cv:   Generates the `cv` PDF file.'
	@echo ''
	@echo 'Utility goals:'
	@echo '  - clean: Deletes all generated PDF files.'
	@echo '  - css:   Updates the vendor CSS files.'
	@echo '  - open:  Open the generated PDF files.'

# ------------------------------------------------------------------------------
# CI
# ------------------------------------------------------------------------------

.PHONY: ci
ci:
	@$(DOCKER_HERE) sdwolfz/eclint:latest make lint/eclint
	@$(DOCKER_HERE) sdwolfz/yamllint:latest make lint/yamllint

.PHONY: lint/eclint
lint/eclint:
	@eclint check $(git ls-files)

.PHONY: lint/yamllint
lint/yamllint:
	@yamllint .

# ------------------------------------------------------------------------------
# Shell goals
# ------------------------------------------------------------------------------

# # shell
#
# Open a shell in the `chromium` docker image.
#
# Usage:
#
# ```sh
# make shell
# ```
.PHONY: shell
shell:
	@$(DOCKER_HERE) --privileged sdwolfz/chromium:latest sh

# # shell/build
#
# Chains `build` inside a chromium docker container.
#
# Usage:
#
# ```sh
# make shell/build
# ```
.PHONY: shell/build
shell/build:
	@$(DOCKER_HERE) --privileged sdwolfz/chromium:latest make build

# # shell/css
#
# Open a shell in the `csso-cli` docker image.
#
# Usage:
#
# ```sh
# make shell/css
# ```
.PHONY: shell/css
shell/css:
	@$(DOCKER_HERE) sdwolfz/csso-cli:latest sh

# ------------------------------------------------------------------------------
# Build goals
# ------------------------------------------------------------------------------

# # build
#
# Generates all PDF files.
#
# Usage:
#
# ```sh
# make build
# ```
.PHONY: build
build: build/card build/cv

# # build/card
#
# Generates the `card` PDF file.
#
# Usage:
#
# ```sh
# make build/card
# ```
.PHONY: build/card
build/card:
	@chromium-browser $(CHROMIUM_FLAGS) --print-to-pdf=card.pdf ./card.html

# # build/cv
#
# Generates the `cv` PDF file.
#
# Usage:
#
# ```sh
# make build/cv
# ```
.PHONY: build/cv
build/cv:
	@chromium-browser $(CHROMIUM_FLAGS) --print-to-pdf=cv.pdf ./cv.html

# ------------------------------------------------------------------------------
# Utility goals
# ------------------------------------------------------------------------------

# # clean
#
# Deletes all generated PDF files.
#
# Usage:
#
# ```sh
# make clean
# ```
.PHONY: clean
clean: clean/card clean/cv clean/tmp

.PHONY: clean/card
clean/card:
	@rm -f card.pdf

.PHONY: clean/cv
clean/cv:
	@rm -f cv.pdf

.PHONY: clean/tmp
clean/tmp:
	@rm -rf tmp

# # css
#
# Updates the vendor CSS files.
#
# Usage:
#
# ```sh
# make css
# ```

.PHONY: css
css: css/fontawesome css/normalize css/paper css/roboto

.PHONY: css/fontawesome
css/fontawesome:
	@sh ./scripts/fontawesome.sh

.PHONY: css/normalize
css/normalize:
	@sh ./scripts/normalize.sh

.PHONY: css/paper
css/paper:
	@sh ./scripts/paper.sh

.PHONY: css/roboto
css/roboto:
	@sh ./scripts/roboto.sh

# # open
#
# Open the generated PDF files.
#
# Usage:
#
# ```sh
# make open
# ```
.PHONY: open
open: open/card open/cv

.PHONY: open/card
open/card:
	@xdg-open card.pdf

.PHONY: open/cv
open/cv:
	@xdg-open cv.pdf

.PHONY: open/html
open/html:
	@xdg-open cv.html
